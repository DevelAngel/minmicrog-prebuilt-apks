# MinMicroG Prebuilt APKs 

Integration of MinMicroG in the build process of Lineage OS as prebuilt APKs.

Note that this project provides only the Android.mk/Android.bp and uses [MinMicroG](https://github.com/FriendlyNeighborhoodShane/MinMicroG) as prebuilt APK downloader.